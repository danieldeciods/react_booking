import React from 'react';
import ReactDOM from 'react-dom';
import App from './App'

//import the Bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css'

//render components to our root div
ReactDOM.render(

		<App />, document.getElementById('root')

	)