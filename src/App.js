import React from 'react';
import './App.css';

// Import Navbar from our reusable component
import Navbar from './components/Navbar';
import Banner from './components/Banner';
import Highlights from './components/Highlights';
import Home from './pages/Home'

import { Container } from 'react-bootstrap'


export default function App(){
  return (

      <>
        <Navbar />
        <Container>
          <Home />         
        </Container>
      </>

    )
}